from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

chrome_options = Options()
chrome_options.add_experimental_option("detach", True)

driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://amazon.fr")

driver.find_element(By.ID, "sp-cc-accept").click()

try:
    search_box = driver.find_element(By.ID, "twotabsearchtextbox")
    search_box.clear()
    search_box.send_keys("Harry Potter L'héritage ps5")
    search_box.send_keys(Keys.ENTER)

    price = driver.find_element(By.XPATH,"//*[@id=\"search\"]/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/div/div/div[3]/div[3]/div[2]/a/span/span[2]/span[1]")
    print(price.text)
    # driver.find_element(By.ID, "nav-search-submit-button").click()
except Exception as ex:
    assert False
# driver.quit()