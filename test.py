# Exercice 1
# a = 23
# b = 0
# resultat = a / b

# Exercice 2
# tableau = ['a', 'b']
# print(tableau[2])

# Exercice 3
# nombres = {
#     1: "un",
#     2: "deux"
# }
# print(nombres[3])

# Exercice 4
nom = "test.txt"
try : # essayer
    f = open(nom, 'r')
    print(f.read())
    a = 12 / 0
# except si une exception est levée
except FileNotFoundError:
    print("Fichier n'existe pas !")
except: # Traitement de toutes les erreurs
    print("Oups une erreur s'est produite !")
finally: # Terminaison avec succès
    f.close()
    print("Le fichier est bien fermé !")

# Exercice 5
# texte = "a"
# int(texte)