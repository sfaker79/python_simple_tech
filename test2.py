class DiscoNotAllowedException(Exception):
    
    def __init__(self):
        super().__init__("Vous n'avez pas le droit d'entrer !")

def entrerDisco(age):
    if age > 18:
        print("Bienvenu")
    else:
        # Lever l'exception (raise)
        raise DiscoNotAllowedException()


if __name__ == "__main__":
    entrerDisco(20)
    try:
        entrerDisco(15)
    except DiscoNotAllowedException:
        print("Désolé vous n'avez pas le droit d'entrer car vous êtes mineur !")
    entrerDisco(34)
