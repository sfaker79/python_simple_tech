class Voiture:
    # Fonction d'inititalisation
    def __init__(self, couleur, marque, vitesse=0):
        self.couleur = couleur
        self.marque = marque
        self.vitesse = vitesse

    def accelerer(self, vitesse):
        self.vitesse += vitesse

    def frainer(self, vitesse):
        self.vitesse -= vitesse
        if self.vitesse < 0:
            self.vitesse = 0

voiture_bleu = Voiture("bleu", "Renault", 200)
voiture_rouge = Voiture("rouge", "Peugeot", 100)

print(voiture_bleu.couleur)
print(voiture_bleu.marque)
print(voiture_bleu.vitesse)

print('-'*10)

print(voiture_rouge.couleur)
print(voiture_rouge.marque)
print(voiture_rouge.vitesse)

voiture_bleu.accelerer(10)
print(voiture_bleu.vitesse)