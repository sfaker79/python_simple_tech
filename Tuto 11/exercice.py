# Créer une classe étudiant
# Attributs : 
#   Nom
#   Prenom
#   Notes reçues aux examens
# Initialiser les objets avec :
#   Un nom donné
#   Un prénom donné
#   Une liste d'examens vide
# Méthode :
#   Doit imprimer la moyenne de l'étudiant
#   Si la liste est vide elle imprime "L'étudiant suivant {{nom}}" n'a pas encore complété d'éxamens

class Etudiant:
    def __init__(self, nom, prenom, notes = []):
        self.nom = nom
        self.prenom = prenom
        self.notes = notes
        
    def moyenne(self):
        return sum(self.notes) / len(self.notes)
    
    def imprimer_moyenne(self):
        if(len(self.notes) == 0):
            print(f"L'étudiant {self.nom} n'a pas encore complété d'examens !")
        else:
            print(f"La moyenne de l'étudiant {self.nom} est : {self.moyenne()}") 


    # def moyenne(self):
    #     if(len(self.notes) == 0):
    #         print(f"L'étudiant {self.nom} n'a pas encore complété d'examens !")
    #     else:
    #         moy = 0
    #         for elem in self.notes:
    #             moy += elem
    #         moy = moy / len(self.notes)
    #         print(f"La moyenne de l'étudiant {self.nom} est : {moy}")

# etudiant1 = Etudiant('soufiane', 'faker', [])
# etudiant1.imprimer_moyenne()

etudiant_paul = Etudiant('Paul', 'Legrand')
etudiant_paul.imprimer_moyenne()
etudiant_paul.notes.append(18)
etudiant_paul.notes.append(20)
etudiant_paul.imprimer_moyenne()
etudiant_paul.notes.append(1)
etudiant_paul.imprimer_moyenne()
