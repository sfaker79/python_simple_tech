from functools import wraps

def log_func(func):

    @wraps(func)
    def ajouter_logs(*args, **kwargs):
        print(f"Avant la fonction {func.__name__}")
        resultat = func(*args, **kwargs)
        print(f"Après la fonction {func.__name__}")
        return resultat
    return ajouter_logs

def repeter(nbr_iterations):
    
    def decorateur(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            resultats = []
            for i in range(nbr_iterations):
                resultats.append(func(*args, **kwargs))
            return resultats

        return wrapper

    return decorateur

@log_func
@repeter(5)
def bonjour(nom, age=18, taille=1.70):
    print(f"Bonjour {nom}, tu as {age} ans et une taille de {taille} m")

@log_func
def add(a, b):
    resultat = a + b
    print(f"{a} + {b} = {resultat}")
    return resultat

@log_func
def sub(a, b):
    resultat = a - b
    print(f"{a} - {b} = {resultat}")
    return resultat

# bonjour("Soufiane", taille=1.8)
# print(f"Le résultat de add(3,4) = {add(3,4)}")
# print(f"Le résultat de sub(13,4) = {sub(13,4)}")

# a = bonjour
# a("Paul")

# def ma_fonction(func):
#     return func(12, 3)

# Appel de la fonction bonjour à l'intérieur de ma_fonction
# ma_fonction(bonjour)
# ma_fonction(add)
# ma_fonction(sub)

# def ma_fonction(func):

#     def ajouter_logs():
#         print(f"Avant la fonction {func.__name__}")
#         resultat = func(12, 3)
#         print(f"Après la fonction {func.__name__}")
#         return resultat
#     return ajouter_logs

# add = log_func(add)
# sub = log_func(sub)
# sera remplacer par @log_func

# print(add(12, 3))
# print(sub(12, 3))
# bonjour("Hiba", age=15)

# print(add.__name__)

bonjour("Anes", age=8, taille=1.5)